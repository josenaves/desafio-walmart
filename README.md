# Desafio Walmart #

O Walmart esta desenvolvendo um novo sistema de logistica e sua ajuda é muito importante neste momento. 

Sua tarefa será desenvolver o novo sistema de entregas visando sempre o menor custo. 

Para popular sua base de dados o sistema precisa expor um webservice que aceite o formato de malha logística (exemplo abaixo). 
Nesta mesma requisição o requisitante deverá informar um nome para este mapa. 

É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam. 

O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.

A B 10

B D 15

A C 20

C D 30

B E 50

D E 30


Com os mapas carregados o requisitante irá procurar o menor valor de entrega e seu caminho, para isso ele passará o mapa, nome do ponto de origem, nome do ponto de destino, autonomia do caminhão (km/l) e o valor do litro do combustivel.

Agora sua tarefa é criar este webservice. 

Um exemplo de entrada seria, mapa SP, origem A, destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D com custo de 6,25.


### Tecnologias utilizadas ###

* Java 8
* [Spring Boot](http://projects.spring.io/spring-boot/)
* [Algoritmo Dijkstra Shortest Path (with priority queue)](http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Using_a_priority_queue)
* [Gradle](http://gradle.org/)

### Serviços ###

Este projeto expõe 2 serviços REST - um para manutenção de rota e outro para consultar a melhor rota.

GET /maps

Retorna a lista de mapas persistidos.

POST /map

Persiste um mapa

POST /map
{
	"name" : "SP",
	"segments" : [
		{ "origin" : "A", "destination" : "B", "distance" : "10" },
		{ "origin" : "B", "destination" : "D", "distance" : "15" },
		{ "origin" : "A", "destination" : "C", "distance" : "20" },
		{ "origin" : "C", "destination" : "D", "distance" : "30" },
		{ "origin" : "B", "destination" : "E", "distance" : "50" },
		{ "origin" : "D", "destination" : "E", "distance" : "30" }
	]
}

----

POST /map
{
	"name" : "MG",
	"segments" : [
		{ "origin" : "Guaxupé", "destination" : "Guaranésia", "distance" : "10" },
		{ "origin" : "Guaxupé", "destination" : "Tapiratiba", "distance" : "20" },
		{ "origin" : "Guaxupé", "destination" : "Muzambinho", "distance" : "28" },
		{ "origin" : "Guaranésia", "destination" : "Arceburgo", "distance" : "20" },
		{ "origin" : "Arceburgo", "destination" : "Mococa", "distance" : "20" }
	]
}

----

GET /best + query string parameters

Exemplo:

GET "http://localhost:8080/best?map=SP&origin=A&destination=E&autonomy=10&fuel=2.5"

{
	"route": "A B D E",
	"cost": 13.75
}


### Como executar ###

Primeiro, compile o projeto com:

> gradle build 

Depois, execute com:

> gradle run

Se quiser executar os testes de integração:

> gradle test


### Observações ###

O projeto inicia com um mapa previamente carregado (name="SP").

O arquivo src/main/resources/data.sql contém uma carga inicial.

Novos mapas podem ser incluídos neste arquivo.

Mapas criados em tempo de execução serão válidos apenas para aquela sessão.
Para mudar isso, é necessário criar uma tabela em um banco de dados e definir um datasource para ela no arquivo application.properties

A tabela deve se chamar MAP e possuir os seguintes campos:

id integer,
name varchar(255),
origin varchar(255),
destination varchar(255),
distance long



