
insert into map (name, origin, destination, distance) values ('SP', 'A', 'B', 10);
insert into map (name, origin, destination, distance) values ('SP', 'B', 'D', 15);
insert into map (name, origin, destination, distance) values ('SP', 'A', 'C', 20);
insert into map (name, origin, destination, distance) values ('SP', 'C', 'D', 30);
insert into map (name, origin, destination, distance) values ('SP', 'B', 'E', 50);
insert into map (name, origin, destination, distance) values ('SP', 'D', 'E', 30);

insert into map (name, origin, destination, distance) values ('SP01', 'Jundiaí', 'Itupeva', 20);
insert into map (name, origin, destination, distance) values ('SP01', 'Jundiaí', 'São Paulo', 60);
insert into map (name, origin, destination, distance) values ('SP01', 'Jundiaí', 'Atibaia', 70);
insert into map (name, origin, destination, distance) values ('SP01', 'Jundiaí', 'Campinas', 40);
insert into map (name, origin, destination, distance) values ('SP01', 'Itupeva', 'Itu', 30);
insert into map (name, origin, destination, distance) values ('SP01', 'Itu', 'Sorocaba', 25);
insert into map (name, origin, destination, distance) values ('SP01', 'Sorocaba', 'Barueri', 70);
insert into map (name, origin, destination, distance) values ('SP01', 'Barueri', 'São Paulo', 30);
insert into map (name, origin, destination, distance) values ('SP01', 'São Paulo', 'Atibaia', 70);
insert into map (name, origin, destination, distance) values ('SP01', 'Atibaia', 'São José dos Campos', 90);
insert into map (name, origin, destination, distance) values ('SP01', 'Atibaia', 'São José dos Campos', 90);

