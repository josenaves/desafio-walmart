package com.josenaves.walmart.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.josenaves.walmart.domain.Map;

/**
 * Repositório responsável por métodos de persistência da entidade Map.
 * @author josenaves@gmail.com
 */
public interface MapRepository extends JpaRepository<Map, Integer>  {
	List<Map> findByName(String name);
}
