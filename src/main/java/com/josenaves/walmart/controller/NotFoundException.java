package com.josenaves.walmart.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exceção retornada quando não se encontra um mapa ou cidade. 
 * @author josenaves@gmail.com
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND)  // 404
public class NotFoundException extends RuntimeException{
	private static final long serialVersionUID = -3040566460773189657L;
	
	public NotFoundException(){
		super();
	}
	
	public NotFoundException(String msg) {
		super(msg);
	}
}
