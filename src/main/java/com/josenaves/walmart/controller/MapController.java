package com.josenaves.walmart.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.josenaves.walmart.domain.Map;
import com.josenaves.walmart.repository.MapRepository;
import com.josenaves.walmart.service.RouteService;

/**
 * Ponto de entrada serviço criação e consulta de mapas. 
 * @author josenaves@gmail.com
 */
@RestController
public class MapController  {
	
	@Autowired
	private MapRepository repo;
	
	@Autowired
	private RouteService service;
	
	@RequestMapping(value="/map", method=RequestMethod.POST)
	public void saveMap(@RequestBody MapRequest map){

		String name = map.getName();
		
		if (name == null) throw new IllegalArgumentException("Please inform the maps's name!");
		
		for (MapSegment segment : map.getSegments() ) {
			Map route = new Map(name, segment.getOrigin(), segment.getDestination(), segment.getDistance());
			repo.save(route);
		}
		
		service.loadMaps();
	}
	
	@RequestMapping(value="/maps", method=RequestMethod.GET)
	public List<MapRequest> getMaps(){
		List<Map> maps = repo.findAll();

		List<MapRequest> listOut = new ArrayList<MapRequest>();
		
		for (Map m : maps) {
			MapSegment seg = new MapSegment(m.getOrigin(), m.getDestination(), m.getDistance());
			findMap(listOut, m.getName(), seg);
		}
		
		return listOut;
	}
	
	@RequestMapping(value="/map/{name}", method=RequestMethod.GET)
	public MapRequest getMap(@PathVariable String name){
		
		List<Map> map = repo.findByName(name);
		
		if (map.isEmpty()) throw new NotFoundException("Map not found!");
		
		MapRequest mapOut = new MapRequest();
		mapOut.setName(name);
		
		for (Map m : map) {
			MapSegment seg = new MapSegment(m.getOrigin(), m.getDestination(), m.getDistance());
			mapOut.getSegments().add(seg);
		}
		
		return mapOut;
	}
	
	private void findMap(List<MapRequest> map, String name, MapSegment seg) {
		for (int i = 0; i < map.size(); i++) {
			if (map.get(i) != null) {
				if (map.get(i).getName() != null) {
					if (map.get(i).getName().equals(name)) {
						// adiciona o segmento
						map.get(i).getSegments().add(seg);
						return;
					}
				}
			}
		}
		
		// nao encontrou, cria um novo mapa e adiciona o segmento 
		MapRequest mr = new MapRequest(name, new ArrayList<MapSegment>());
		mr.getSegments().add(seg);
		map.add(mr);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	private void handleBadRequests(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.BAD_REQUEST.value(), "Please try again and with a non empty arguments");
	}
	
}