package com.josenaves.walmart.controller;

import java.io.Serializable;

/**
 * Representa uma requisição de rota.
 * @author josenaves@gmail.com
 */
public class RouteRequest implements Serializable {

	//{ "map" : "A", "origin": "A", "destination":"E", "autonomy": "10", "fuel": "2.5"	}
	
	private static final long serialVersionUID = 201506021709L;
	
	private String map;
	private String origin;
	private String destination;
	private Double autonomy;
	private Double fuel;
	
	public RouteRequest() {
	}
	
	public void validate() throws IllegalArgumentException {
		//  validate the fields
		if (map == null | map.equals("") | 
			origin == null | origin.equals("") |
			destination == null | destination.equals("") | 
			autonomy == null | autonomy.equals("") |
			fuel == null | fuel.equals("")) {

			throw new IllegalArgumentException("You need to specify all field: map, origin, destination, autonomy, fuel");
		}
	}
	

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Double getAutonomy() {
		return autonomy;
	}

	public void setAutonomy(Double autonomy) {
		this.autonomy = autonomy;
	}

	public Double getFuel() {
		return fuel;
	}

	public void setFuel(Double fuel) {
		this.fuel = fuel;
	}

	@Override
	public String toString() {
		return "RouteRequest [map=" + map + ", origin=" + origin
				+ ", destination=" + destination + ", autonomy=" + autonomy
				+ ", fuel=" + fuel + "]";
	}
	
}
