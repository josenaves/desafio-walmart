package com.josenaves.walmart.controller;

import java.io.Serializable;

/**
 * Representa a resposta do serviço de melhor rota.
 * @author josenaves@gmail.com
 */
public class RouteResponse implements Serializable {

	private static final long serialVersionUID = 7524755496496123636L;
	
	private String route;
	private double cost;
	
	public RouteResponse() {
	}
	
	public RouteResponse(String route, double cost) {
		super();
		this.route = route;
		this.cost = cost;
	}
	
	public String getRoute() {
		return route;
	}
	
	public double getCost() {
		return cost;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cost);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((route == null) ? 0 : route.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RouteResponse other = (RouteResponse) obj;
		if (Double.doubleToLongBits(cost) != Double
				.doubleToLongBits(other.cost))
			return false;
		if (route == null) {
			if (other.route != null)
				return false;
		} else if (!route.equals(other.route))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RouteResponse [route=" + route + ", cost=" + cost + "]";
	}
	
	
}
