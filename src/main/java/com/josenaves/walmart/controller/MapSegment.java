package com.josenaves.walmart.controller;

import java.io.Serializable;

/**
 * Representa um segmento na rota de um mapa.
 * @author josenaves@gmail.com
 */
public class MapSegment implements Serializable {

	private static final long serialVersionUID = -8023799216787314060L;

	private String origin;
	private String destination;
	private Integer distance;
	
	public MapSegment(){}
	
	public MapSegment(String origin, String destination, Integer distance) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.distance = distance;
	}
	
	public String getOrigin() {
		return origin;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public Integer getDistance() {
		return distance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((destination == null) ? 0 : destination.hashCode());
		result = prime * result
				+ ((distance == null) ? 0 : distance.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapSegment other = (MapSegment) obj;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} else if (!destination.equals(other.destination))
			return false;
		if (distance == null) {
			if (other.distance != null)
				return false;
		} else if (!distance.equals(other.distance))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RouteSegment [origin=" + origin + ", destination="
				+ destination + ", distance=" + distance + "]";
	}

}
