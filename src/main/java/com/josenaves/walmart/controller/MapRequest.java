package com.josenaves.walmart.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Representa uma requisição ao serviço de maps
 * @author josenaves@gmail.com
 */
public class MapRequest implements Serializable {

	private static final long serialVersionUID = 7816049074324328077L;

	private String name;
	private List<MapSegment> segments = new ArrayList<MapSegment>();
	
	public MapRequest() {
	}

	public MapRequest(String name, List<MapSegment> segments) {
		super();
		this.name = name;
		this.segments = segments;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<MapSegment> getSegments() {
		return segments;
	}

	public void setSegments(List<MapSegment> segments) {
		this.segments = segments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((segments == null) ? 0 : segments.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MapRequest other = (MapRequest) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (segments == null) {
			if (other.segments != null)
				return false;
		} else if (!segments.equals(other.segments))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RouteRequest [name=" + name + ", segments=" + segments + "]";
	}
}
