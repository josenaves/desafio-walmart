package com.josenaves.walmart.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.josenaves.walmart.repository.MapRepository;
import com.josenaves.walmart.service.RouteService;
import com.josenaves.walmart.service.RouteServiceImpl;

/**
 * Ponto de entrada serviço cálculo de melhor rota. 
 * @author josenaves@gmail.com
 */
@RestController
public class BestRouteController  {
	
	@Autowired
	private RouteService routeService;
	
	@Autowired
	private MapRepository repo;

    private static Logger log = Logger.getLogger(BestRouteController.class);

	
	// GET http://localhost:8080/best?map=SP&origin=A&destination=E&autonomy=10&fuel=2.5
	// Response		//{ "route" : "A B D", "distance": 25, "cost:" : "6.25" }
	@RequestMapping(value="/best", method=RequestMethod.GET)
	public @ResponseBody RouteResponse getBestRoute(
			@RequestParam String map,
			@RequestParam String origin,
			@RequestParam String destination,
			@RequestParam String autonomy,
			@RequestParam String fuel) {
		
		if (map == null | map.equals("") | 
			origin == null | origin.equals("") |
			destination == null | destination.equals("") | 
			autonomy == null | autonomy.equals("") |
			fuel == null | fuel.equals("")) {
			throw new IllegalArgumentException("You need to specify all field: map, origin, destination, autonomy, fuel");
		}
			
		double dAutonomy, dFuel;
		try {
			// try to convert the numbers
			dAutonomy = Double.parseDouble(autonomy);
			dFuel = Double.parseDouble(fuel);
		}
		catch (Exception e) {
			throw new IllegalArgumentException("Use numbers for autonomy and fuel");
		}
		
		// call Dijkstra algorithm
		RouteResponse resp = routeService.getShortestPath(map, origin, destination, dAutonomy, dFuel);
		
		log.debug("Response: " + resp);
		
		return resp;
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	private void handleBadRequests(HttpServletResponse response) throws IOException {
	    response.sendError(HttpStatus.BAD_REQUEST.value(), "Please try again and with a non empty arguments");
	}
}