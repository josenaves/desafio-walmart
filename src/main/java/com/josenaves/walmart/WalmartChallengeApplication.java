package com.josenaves.walmart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.josenaves.walmart.repository.MapRepository;
import com.josenaves.walmart.service.RouteService;

// Tell Spring to automatically inject any dependencies that are marked in our classes with @Autowired
@EnableAutoConfiguration
// Tell Spring to automatically create a JPA implementation of our RouteRepository
@EnableJpaRepositories(basePackageClasses = {MapRepository.class})
// Tell Spring to turn on WebMVC (e.g., it should enable the DispatcherServlet so that requests can be routed to our Controllers)
@EnableWebMvc
// Tell Spring that this object represents a Configuration for the application
@Configuration
// Tell Spring to go and scan our controller package (and all sub packages) to
// find any Controllers or other components that are part of our applciation.
// Any class in this package that is annotated with @Controller is going to be
// automatically discovered and connected to the DispatcherServlet.
@ComponentScan
@SpringBootApplication
/**
 * Ponto de entrada da aplicação.
 * @author josenaves@gmail.com
 */
public class WalmartChallengeApplication implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private RouteService routeService;
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(WalmartChallengeApplication.class, args);
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		routeService.loadMaps();
	}
	
}