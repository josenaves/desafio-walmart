package com.josenaves.walmart.domain;

/**
 * Representa a ligação entre um vértice (cidade) de um grafo.
 * @author josenaves@gmail.com
 */
public final class Edge {
	
	private Vertex from;
	private Vertex to;
	private Integer distance;

	public Edge(Vertex from, Vertex to, Integer distance) {
		this.from = from;
		this.to = to;
		this.distance = distance;
		
		this.from.getNeighbors().add(to);
		this.from.getEdges().add(this);
		
		this.to.getNeighbors().add(from);
		this.to.getEdges().add(this);
	}

	public Vertex getFrom() {
		return from;
	}

	public Vertex getTo() {
		return to;
	}

	public Integer getDistance() {
		return distance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((distance == null) ? 0 : distance.hashCode());
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (distance == null) {
			if (other.distance != null)
				return false;
		} else if (!distance.equals(other.distance))
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Edge [from=" + from + ", to=" + to + ", distance=" + distance + "]";
	}

}
