package com.josenaves.walmart.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa o vértice (cidade) de um grafo.
 * @author josenaves@gmail.com
 */
public class Vertex {
	
	private String name;
	
	private List<Vertex> neighbors = new ArrayList<Vertex>();
	private List<Edge> edges  = new ArrayList<Edge>();
	
	public Vertex(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public List<Vertex> getNeighbors() {
		return neighbors;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Vertex [name=" + name + "]";
	}

}
