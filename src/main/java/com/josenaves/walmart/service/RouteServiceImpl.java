package com.josenaves.walmart.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.josenaves.walmart.controller.NotFoundException;
import com.josenaves.walmart.controller.RouteResponse;
import com.josenaves.walmart.domain.Edge;
import com.josenaves.walmart.domain.Graph;
import com.josenaves.walmart.domain.Vertex;
import com.josenaves.walmart.repository.MapRepository;

/**
 * Implementação dos métodos do serviço de rotas.
 * @author josenaves@gmail.com
 */
@Service
public class RouteServiceImpl implements RouteService {

    private static Logger log = Logger.getLogger(RouteServiceImpl.class);
    
	private static final long INFINITE = 100000l;
	
	@Autowired
	private MapRepository repo;
	
	private Map<String, Graph> graphs;
	
	private class VertexInfo {
		String name;
		Long distance;
		Vertex predecessor;
		
		public VertexInfo(String name, Long distance, Vertex predecessor) {
			this.name = name;
			this.distance = distance;
			this.predecessor = predecessor;
		}

		@Override
		public String toString() {
			return "VertexInfo [name=" + name + ", distance=" + distance
					+ ", predecessor=" + predecessor + "]";
		}
	}
	
	
	@Override
	public void loadMaps() {
		List<com.josenaves.walmart.domain.Map> maps = repo.findAll();
		
		if (!maps.isEmpty()) {
			graphs = new HashMap<String, Graph>();	
		}
		
		for (com.josenaves.walmart.domain.Map map : maps) {
			
			Graph graph;
			if (graphs.containsKey(map.getName())) {
				graph = graphs.get(map.getName());
			}
			else {
				graph = new Graph();
				graphs.put(map.getName(), graph);
			}
			
			// get or make the vertices
			Vertex origin = graph.getVertex(map.getOrigin()); 
			if (origin == null) origin = new Vertex(map.getOrigin());
			
			Vertex destination = graph.getVertex(map.getDestination());
			if (destination == null) destination = new Vertex(map.getDestination());
			
			// make de edge
			Edge edge = new Edge(origin, destination, map.getDistance());
			
			graph.getEdges().add(edge);
			graph.getVertices().put(origin.getName(), origin);
			graph.getVertices().put(destination.getName(), destination);
		}
		
		log.debug("DONE : " + graphs);
	}

	@Override
	public RouteResponse getShortestPath(String map, String origin, String destination, double autonomy, double cost) {
		
		HashMap<String, VertexInfo> info = shortestPath(map, origin, destination);
		
		log.debug(info);

		// get the destination node
		VertexInfo v = info.get(destination);
		
		// calculate the cost
		double total = (v.distance / autonomy) * cost; 

		// get the route
		Stack<String> stack = new Stack<String>();
		while (!v.name.equals(origin)) {
			stack.push(v.name);
			v = info.get(v.predecessor.getName());
		}
		stack.push(v.name);
		
		StringBuffer r = new StringBuffer();
		while (!stack.empty()) {
			r.append(stack.pop()).append(" ");
		}
		
		RouteResponse route = new RouteResponse(r.toString().trim(), total);
		return route;
	}
	
	/**
	 * @param graph
	 * @param source
	 */
	private HashMap<String, VertexInfo> shortestPath(String map, String origin, String destination) {
		
		Graph graph = graphs.get(map);
		if (graph == null) throw new NotFoundException("Map not found!");
		
		log.debug("Graph = " + graph);
	
		if (!graph.containsVertix(origin)) throw new NotFoundException("Origin not found!");
		if (!graph.containsVertix(destination)) throw new NotFoundException("Destination not found!");
		
		HashMap<String, VertexInfo> info = new HashMap<String, VertexInfo>();
		for (Vertex v : graph.getVertices().values()) {
			info.put(v.getName(), new VertexInfo(v.getName(), INFINITE, null));
		}
		info.get(origin).distance = 0l;
		
		PriorityQueue<VertexInfo> pq = new PriorityQueue<VertexInfo>(11, 
				new Comparator<VertexInfo>() {
					@Override
					public int compare(VertexInfo o1, VertexInfo o2) {
						if (o2.distance == null) return 1;
						if (o1.distance == null) return -1;
						if (o1.distance.equals(o2.distance)) return 0;
						return (int)(o1.distance - o2.distance);
					}
		});
		
		for (Vertex v : graph.getVertices().values()) {
			pq.add(info.get(v.getName()));
		}
		
		while (!pq.isEmpty()) {
			VertexInfo uInfo = pq.remove();
			Vertex uVertex = graph.getVertex(uInfo.name);
			
			log.debug(uVertex.getName() +  " neighbors: " + uVertex.getNeighbors());

			// get all vertex neighbors v of u
			for (Edge v : uVertex.getEdges()) {
				String vName = v.getTo().getName();
				VertexInfo vInfo = info.get(vName);
				
				long weight = v.getDistance();
				long newDistance = (uInfo.distance == INFINITE ? 0 : uInfo.distance) + weight;
				
				if (newDistance < vInfo.distance)  {
					vInfo.distance = newDistance;
					vInfo.predecessor = uVertex;
					updateQueue(pq, vInfo);
				}
			}
		}
		
		return info;
	}
	
	private void updateQueue(PriorityQueue<VertexInfo> pq, VertexInfo v) {
		pq.remove(v);
		pq.add(v);
	}
}
