package com.josenaves.walmart.service;

import com.josenaves.walmart.controller.RouteResponse;

/**
 * Métodos do serviço de rotas.
 * @author josenaves@gmail.com
 */
public interface RouteService {
	void loadMaps();
	RouteResponse getShortestPath(String map, String origin, String destination, double autonomy, double cost);
}
