package com.josenaves.walmart.test;
import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;
import com.josenaves.walmart.WalmartChallengeApplication;
import com.josenaves.walmart.controller.MapRequest;
import com.josenaves.walmart.controller.MapSegment;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WalmartChallengeApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class MapControllerTest {

	@Value("${local.server.port}")
	private int port;
	
	private MapRequest mapMG;
	
	@Before
	public void setUp() {
		RestAssured.port = port;
		
		mapMG = new MapRequest();
		mapMG.setName("MG");
		mapMG.getSegments().add(new MapSegment("Belo Horizonte", "Governador Valadares", 316));
	}
	
	@Test
	public void canFetchMapSP() {
        get("/map/{name}", "SP").
        	then().
                statusCode(HttpStatus.SC_OK).
                body("name", Matchers.is("SP"));
	}
	
	@Test
	public void canFetchMapSP01() {
        get("/map/{name}", "SP01").
        	then().
                statusCode(HttpStatus.SC_OK).
                body("name", Matchers.is("SP01"));
	}
	
	@Test
	public void canCreateMapMG() {
		given().
			body(mapMG).
			contentType("application/json; charset=UTF-16").
		when().
			post("/map").
		then().
			statusCode(200);
	}
}
