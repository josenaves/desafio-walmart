package com.josenaves.walmart.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;
import com.josenaves.walmart.WalmartChallengeApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WalmartChallengeApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class BestControllerTest {

	@Value("${local.server.port}")
	private int port;
	
	@Before
	public void setUp() {
		RestAssured.port = port;
	}
	
	@Test
	public void canFetchBestSP() {
        given().
        	parameters("map", "SP").
        	parameters("origin", "A").
        	parameters("destination", "E").
        	parameters("autonomy", "10").
        	parameters("fuel", "2.5").
    	expect().
        	statusCode(HttpStatus.SC_OK).
        	body("route", equalTo("A B D E")).
        	body("cost", is(13.75f)).
        when().
        	get("/best");
	}
	
	@Test
	public void canFetchBestSP01() {
        given().
        	parameters("map", "SP01").
        	parameters("origin", "Jundiaí").
        	parameters("destination", "São José dos Campos").
        	parameters("autonomy", "10").
        	parameters("fuel", "2.5").
    	expect().
        	statusCode(HttpStatus.SC_OK).
        	body("route", equalTo("Jundiaí Atibaia São José dos Campos")).
        	body("cost", is(40f)).
        when().
        	get("/best");
	}
	
}